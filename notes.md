## Data modeling (1)
#### Database Management System
* is a suite of services (software applications) for managing databases, which
  involves:  
  * allowing simple and **multiple access** to the information
  * **manipulating** the data found in the database (inserting, deleting,
    editing)
  * controls **security** and **integrity**

#### Database
* is an entity in which data can be stored in structured manner, with as little
  redundancy as possible
* gives users access to data, which they can view, enter, or update based on
  users permissions
* handle concurrent access

#### Local Database
* can be used on one machine by one user only

#### Distributed Database
* the information is stored on remote machines and can be accessed over a
  network

#### 3-level Architecture
* external - how users view the data
* conceptual - how programmer plan and implement the DB
* internal - how DBMS stores the data

## DB design (2)
**Data model**
* a description of the objects that could be represented by a computer system
  together with their properties and relationships

**Schema**
* a description of how a database can be designed to represent a data model

**Database**
* an instance of a schema with corresponding data

**Relational DB**
* data is stored in relational tables

## ER Diagrams (3-4)
#### Entity relationships model
* described as three key concepts

#### Entities
* a **uniquely identifiable** objects in the real world about which we wish to
  store data
* grouped together into ‘categories’ called entity types or entity sets
* are instances of a given entity­‐type
* **Weak ET**
  * Depend on other entities to guarantee uniqueness
  * Do not have primary key (attributes) of their own

#### Attributes
* properties that describe an entity (type)
* drawn as ovals, and attached to the boxes representing entity types with
  lines
* can be **atomic** (simple) ![atomic](atomic.png)
* or **composite** ![composite](composite.png)
* **multivalued** attributes store sets of values. e.g. hobbies of person.
  (vector)
* **Key attributes**
  * underlined in ER diagram
  * attribute whose values are unique for each entity in entity type
  * can be **composite**  

#### Relationships
* Captures how two or more entity types are related
* represent the interaction between entity types
* can have attributes
* **degree** of relationship is the number of entity types participating
* **cardinality** specifies the number of entity instances that can participate
  from each side of the relationship of a binary relationship
* **totality**
  * indicated by double line
  * all entities in the entity set must participate in at least one relationship
    in the relationship set

![legend](legend.png)

#### Sub-typing
* subtype is an entity type that inherits the properties from its parent type
* can be **disjoint** (must belong to exactly one subtype) or **inclusive** (may)
  belong to either or both ![subtype](subtype.png)

#### Recursive Relationships
* An entity type can be in a relationship with itself
* For example, one employee manages another

---

**Degree of relation**: The number of attributes of the relation
**Cardinality of relation**: The number of rows/tuples of the relation

A **tuple (record)** is a row of a relation

A **relation schema** is a set of attributes: Student (name: Text, matric: Number, ex1: Number, ex2: Number)

**relational database schema** is a set of relation schemas

A **domain** is a set of atomic values that can be assigned to an attribute

**foreign key** is an attribute (or set of attributes) that exist one or
more tables and which is the primary key for one of those tables.
A value in a foreign key MUST exist in the referenced primary key attribute
The foreign key is used to cross-reference tables

**Integrity constraints**
* Primary key values must be unique
* Primary key values cannot be NULL
* Foreign key values:
    * must exist in the primary key of the referenced relations – we call this referential integrity
    * may be NULL (if it is not a mandatory participation)

**Enterprise constraints**: Application dependent (specified non-key attributes must not be NULL...)

**Referential Integrity**
Concerns the use of Foreign Keys
Guarantees that relationships between tuples are coherent ¡ Every non NULL value in a Foreign Key must also exist in the relation for which it is the Primary Key

Referential Integrity: 3 Strategies
* Restrict – ban any alterations to a primary key if there are foreign key references to it
* Cascade - cascade the effect to all relations in all tables that refer to it
* Set to NULL – allow update in original table, set all corresponding FK values to null


**Set theory** is the branch of mathematics that studies sets
**Sets** are collections of objects

Objects in a set are called **elements** or **members** of a set
Two **sets are equal if** and only if they have the same elements
Given a set S, the **power set** is the set of all subsets of the set S

The **cartesian product** of A and B (A X B) is  the set of all ordered pairs (i.e. tuples) <a,b> where a ∈ A and b ∈ B

A **query** is a sequence of operations applied to relation instances, and the result of a query is also a relation instance

![relation_operations](relation_operations.jpg)

![selection](selection.jpg)

![projection](projection.jpg)

![join](join.jpg)

![natural_join](natural_join.jpg)

![SQL](SQL.jpg)

![basic_query](basic_query.jpg)

![equijoin_query](equijoin_query.jpg)

![selfjoin_query](selfjoin_query.png)

**GROUP BY** allows the aggregations to be applied to groups of rows, according to the grouping of values in a column. It produces as many answers as there are identified groups
